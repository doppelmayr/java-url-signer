package com.rill.util;

/**
 * Typesafe enumeration for hashing algorithms listed in http://docs.oracle.com/javase/7/docs/api/javax/crypto/Mac.html
 */
public enum HashMethod {
    
    HMAC_MD5("HmacMD5"),
    HMAC_SHA256("HmacSHA256"),
    HMAC_SHA1("HmacSHA1");

    private String name;

    private HashMethod(final String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}