package com.rill.util;

import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class UrlParamMap {

    private SortedMap<String, SortedSet<String>> paramValues;

    public UrlParamMap(){
        this.paramValues = new TreeMap<String, SortedSet<String>>();
    }

    public UrlParamMap add(final String parameter, final String value){
        SortedSet values = this.paramValues.get(parameter);
        if(values == null){
            values = new TreeSet();
            this.paramValues.put(parameter, values);
        }
        values.add(value);
        return this;
    }
    
    public SortedSet<String> getValues(final String parameter){
        return this.paramValues.get(parameter);
    }

    //this will actually be sorted
    public Set<String> getParameters(){
        return this.paramValues.keySet();
    }

    public String getSortedParameterString(){
        
        String amp = "";
        
        StringBuilder sb = new StringBuilder();
        for (String paramName : paramValues.keySet()) {
            for (String paramValue : paramValues.get(paramName)){
                sb.append(amp).append(paramName).append("=").append(paramValue);
                amp = "&";
            }
        }
        
        return sb.toString();
    }
}