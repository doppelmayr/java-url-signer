package com.rill.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/**
 * Utility to construct representation of API request URL parameters and generate signature for it. 
 * Allows muptiple parameter values, &param=value1&param=value2.
 *
 * @author: Reggie Tawes Smith
 */
public class UrlSigner{

    public static class Builder{
        
        UrlSigner signer;
        boolean isMethodSet=false;

        public Builder(){
            signer = new UrlSigner();
        }   

        public Builder withHashMethod(HashMethod method){
            signer.setHashMethod(method);
            isMethodSet=true;
            return this;
        }
        
        public Builder withParamValue(final String parameter, final String value){
            signer.addParam(parameter, value);
            return this;
        }

        public UrlSigner build(){
            if(!isMethodSet){
                throw new IllegalStateException("HashMethod is required to sign url string");
            }
            return signer;
        }
    }
    
    private UrlParamMap params;
    private HashMethod method = HashMethod.HMAC_SHA256;
    
    private UrlSigner(){
        this.params = new UrlParamMap();
    }
    
    public String sign(final String secret) throws GeneralSecurityException, UnsupportedEncodingException{
        final String sortedParamStr = this.params.getSortedParameterString();
        return signString(sortedParamStr, secret);
    }
    
    private String signString(String str, String key) throws GeneralSecurityException, UnsupportedEncodingException{
        String result = null;
        //try {
        // Get an hmac_sha256 key from the raw key bytes.
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes("UTF8"), this.method.getName());
                
        // Get an hmac_sha256 Mac instance and initialize with the signing key.
        Mac mac = Mac.getInstance(this.method.getName());
        mac.init(signingKey);
                
        // Compute the hmac on input data bytes.
        byte[] rawHmac = mac.doFinal(str.getBytes("UTF8"));
        
        // Base64-encode the hmac by using the utility in the SDK
        result = Base64.encodeBase64String(rawHmac);
        //}
        //catch (Exception e) {
        //log.error("");
        //}
        
        return result;
    }

    private void setHashMethod(HashMethod method){
        this.method = method;
    }

    private void addParam(final String name, final String value){
        this.params.add(name, value);
    }
}
