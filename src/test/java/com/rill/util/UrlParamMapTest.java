package com.rill.util;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UrlParamMapTest{

    public static final String 
        FIRST_VALUE = "value1",
        SECOND_VALUE = "value2",
        ANOTHER_VALUE = "nothervalue",
        FIRST_KEY = "key1",
        SECOND_KEY = "key2";

    public static final String SORTED_PARAM_STRING = "key1=nothervalue&key2=value1&key2=value2";

    @Test
    public void testAddRetrieve(){
        

        UrlParamMap map = getTestMap();
        
        //assert multiple values are stored/returned
        assertEquals(2, map.getValues(SECOND_KEY).size());
        
        //assert order of values
        String[] orderedValues = new String[]{FIRST_VALUE, SECOND_VALUE};
        int i=0;
        for(String value : map.getValues(SECOND_KEY)){
            assertEquals(orderedValues[i++], value);
        }
        
        //assert order of keys
        String[] orderedKeys = new String[]{FIRST_KEY, SECOND_KEY};
        i=0;
        for(String param : map.getParameters()){
            assertEquals(orderedKeys[i++], param);
        }
    }

    @Test
    public void testGetSortedParameterString(){
        UrlParamMap map = getTestMap();
        String orderedParamString = map.getSortedParameterString();
        assertEquals(SORTED_PARAM_STRING, orderedParamString);
    }

    @Test
    public void testEmptyParamMap(){
        UrlParamMap map = new UrlParamMap();
        assertNotNull(map.getParameters());
        assertEquals(0, map.getParameters().size());
        assertNotNull(map.getSortedParameterString());
        assertEquals("", map.getSortedParameterString());
    }

    UrlParamMap getTestMap(){
        UrlParamMap map = new UrlParamMap();
        map.add(SECOND_KEY, SECOND_VALUE);
        map.add(SECOND_KEY, FIRST_VALUE);
        map.add(FIRST_KEY, ANOTHER_VALUE);
        return map;
    }
        
}
        