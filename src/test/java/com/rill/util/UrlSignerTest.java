package com.rill.util;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class UrlSignerTest{

    public static final String SECRET = "abc";

    @Test
    public void testSignUrl() throws Exception {

        String paramKeyValueString = UrlParamMapTest.FIRST_KEY+"="+UrlParamMapTest.FIRST_VALUE;

        UrlSigner signer = new UrlSigner.Builder()
            .withHashMethod(HashMethod.HMAC_SHA256)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.FIRST_VALUE)
            .build();
        String hashedString = "Uyr7Rej/q+SsK1TCmlZ5wdsVDQVMBkNIUs0eH2kmtLg="; //manually obtained from http://www.online-convert.com/
        assertEquals(hashedString, signer.sign(SECRET));
        

        signer = new UrlSigner.Builder()
            .withHashMethod(HashMethod.HMAC_SHA1)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.FIRST_VALUE)
            .build();
        hashedString = "oezvIin+Icz9QDH1CGeiahYP9vs="; //manually obtained from http://www.online-convert.com/
        assertEquals(hashedString, signer.sign(SECRET));

        signer = new UrlSigner.Builder()
            .withHashMethod(HashMethod.HMAC_MD5)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.FIRST_VALUE)
            .build();
        hashedString = "+M4cKWB9ANive95s8Qo+KQ=="; //manually obtained from http://www.online-convert.com/
        assertEquals(hashedString, signer.sign(SECRET));
        
    }

    @Test
    public void testSignUrlParamOrder() throws Exception {
        UrlSigner signer = new UrlSigner.Builder()
            .withHashMethod(HashMethod.HMAC_SHA256)
            .withParamValue(UrlParamMapTest.SECOND_KEY, UrlParamMapTest.ANOTHER_VALUE)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.SECOND_VALUE)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.FIRST_VALUE)
            .build();
        
        //add parameters in differend order
        UrlSigner anotherSigner = new UrlSigner.Builder()
            .withHashMethod(HashMethod.HMAC_SHA256)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.FIRST_VALUE)
            .withParamValue(UrlParamMapTest.FIRST_KEY, UrlParamMapTest.SECOND_VALUE)
            .withParamValue(UrlParamMapTest.SECOND_KEY, UrlParamMapTest.ANOTHER_VALUE)
            .build();
        
        assertEquals(signer.sign(SECRET), anotherSigner.sign(SECRET));
        
    }

    @Test
    public void testSignEmptyUrl() throws Exception {
        UrlSigner signer = new UrlSigner.Builder().withHashMethod(HashMethod.HMAC_SHA256).build();
        String signed = signer.sign(SECRET);
        assertEquals("4mNgd1BnKaj2Gv8kQTMuQOhEqK1ESJ79gCEOptH1EIg=", signed);
    }

}